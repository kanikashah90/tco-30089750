// Produces Kafka event
'use strict';
const EventEmitter = require('events').EventEmitter;
const util = require('util');
const Kafka = require('node-rdkafka');
const appUtils = require('./utils/helper');
const applicationConfig = appUtils.getApplicationConfig();

// Creates an instance of Kafka Producer with the configuration
// passed in options.
function KafkaProducerImpl(options) {
  const self = this;
  this.brokerList = (options && options.brokerList) ? options.brokerList : applicationConfig.KAFKA_CONSUMER_BROKER_LIST;
  console.log('Broker list is:', self.brokerList);
  this.producer = new Kafka.Producer({
    'metadata.broker.list': self.brokerList,
    'dr_cb': true
  });
  this.producer.connect();
  this.producer.on('ready', function () {
    self.kafkaConnectionSuccessful = true;
    self.emit('producerReady');
  });
  this.producer.on('error', function (err) {
    console.error('[Kafka Producer] Error in producer:' + err);
  });
}

util.inherits(KafkaProducerImpl, EventEmitter);

module.exports.KafkaProducerImpl = KafkaProducerImpl;

// Produces the Kafka records with value passed in 'record' argument.
KafkaProducerImpl.prototype.produce = function (topic, record, key) {
  try {
    if (typeof (record) === 'string')
      record = Buffer.from(record);
    if (typeof (key) === 'string')
      key = Buffer.from(key);
    if (Buffer.isBuffer(record) && this.kafkaConnectionSuccessful && (key && Buffer.isBuffer(key))) {
      this.producer.produce(topic, null, record, key);
      console.log('[Kafka Producer] Produce complete');
    }
  } catch (err) {
    console.error('[Kafka Producer] Error in producing the message:' + err);
  }
  return;
}