'use strict';
var _ = require('lodash');

function ChallengeApiService(httpService) {
  this.httpService = httpService;
}

module.exports.ChallengeApiService = ChallengeApiService;

// Validates the input challenge JSON to make sure none of the required fields to
// create a challenge are missing.
function validateChallenge(challenge, cb) {
  var errString = '';
  if (!challenge.projectId)
    errString += 'projectId is missing';
  if (!challenge.confidentialityType)
    errString += 'confidentialityType is missing';
  if (!challenge.milestoneId)
    errString += 'milestoneId is missing';
  if (!challenge.registrationStartsAt)
    errString += 'registrationStartsAt is missing';
  if (!challenge.reviewType)
    errString += 'reviewType is missing';
  if (!challenge.subTrack)
    errString += 'subTrack is missing';
  if (errString.length)
    cb(errString);
  else
    cb();
}

// Makes the call to the TopcodeAPI to create Challenge
ChallengeApiService.prototype.create = function (challenge, cb) {
  var self = this;
  validateChallenge(challenge, function (err) {
    if (err) {
      cb(err);
      return;
    } else {
      const challengePath = '';
      var errString;
      var challengeCreatePayload = {};
      createChallengePostPayload(challenge, challengeCreatePayload);
      const postBody = {
        param: challengeCreatePayload
      };
      self.httpService.post(challengePath, postBody, {}, function (err, response) {
        if (err) {
          errString = '[Challenge Service] Failed in creating the challenge' + err;
          cb(errString);
          return;
        } else if (!response.result.success) {
          errString = '[Challenge Service] Failed in creating the challenge. Received success false for the api call';
          cb(errString);
          return;
        } else if (response.result.content && !response.result.content.id) {
          errString = '[Challenge Service] Failed in creating the challenge. Challenge id is missing in the challenge creation response';
          cb(errString);
          return;
        }
        cb(null, 'New Challenge create with id ' + response.result.content.id);
        return;
      })
    }
  })
}

// Makes a call to the Topcoder API to get all the challenges
ChallengeApiService.prototype.get = function (cb) {
  const challengePath = '';
  var errString;
  this.httpService.get(challengePath, function (err, response) {
    if (err) {
      errString = '[Challenge Service] Failed in fetching the challenges' + err;
      cb(errString);
      return;
    }
    if (response && response.result && response.result.success && response.result.metadata)
      console.log('[Challenge Service] Challenges fetch complete. Total number of challenges are', response.result.content.length);
    cb();
    return;
  });
}

// Makes a call to the Topcoder API to update a challenge
ChallengeApiService.prototype.update = function (challenge, cb) {
  var errString;
  const self = this;
  if (!challenge.id) {
    errString = '[Challenge Service] Challenge id is missing';
    cb(errString);
    return;
  }
  const challengePath = challenge.id;
  this.httpService.get(challengePath, function (err, challengeDetails) {
    if (err) {
      errString = '[Challenge Service] Failed in fetching the challenge based on kafka challenge id ' + challenge.id;
      cb(errString);
      return;
    }
    if (challengeDetails && challengeDetails.result && !challengeDetails.result.success) {
      errString = '[Challenge Service] Failed in fetching the challenge. API success response is false for ' + challenge.id;
      cb(errString);
      return;
    }
    if (!challengeDetails.result.content)
      challengeDetails.result.content = {}
    var challengePutPayload = {};
    createPutPayload(challengeDetails.result.content, challenge, challengePutPayload);
    const putBody = {
      param: challengePutPayload
    }
    self.httpService.put(challengePath, putBody, {}, function (err, data) {
      if (err) {
        errString = '[Challenge Service] Failed in updating the challenge with id: ' + challenge.id + err;
        cb(errString);
      } else {
        console.info('[Challenge Service] Challenge with id', challenge.id, 'is successfully updated');
        cb(null, data);
      }
      return;
    });
  });
}

// Add the allowed update properties on a challenge to the destination object.
// Args:
// challenge: challege object fetched from get call for the challenge
// update: update received for the challenge from Kafka topic
// destination: final object to create for the challenge update API call
function createPutPayload(challenge, update, destination) {
  // Allowed properties for Challenge Update payload. Created from the Swagger file of the API
  const challengeUpdateFields = ['id', 'confidentialityType', 'technologies', 'subTrack', 'name', 'reviewType', 'billingAccountId', 'milestoneId', 'detailedRequirements', 'submissionGuidelines', 'registrationStartsAt', 'registrationEndsAt', 'checkpointSubmissionStartsAt', 'checkpointSubmissionEndsAt', 'submissionEndsAt', 'round1Info', 'round2Info',
    'platforms', 'numberOfCheckpointPrizes', 'checkpointPrize', 'finalDeliverableTypes', 'prizes',
    'projectId', 'submissionVisibility', 'maxNumOfSubmissions', 'task', 'assignees', 'copilotId', 'copilotFee',
    'failedRegisterUsers', 'environment', 'codeRepo', 'fixedFee', 'percentageFee'];
  challengeUpdateFields.forEach(function (field) {
    if (!challenge[field] && update[field])
      destination[field] = update[field]
    else if (challenge[field] && update[field])
      updateObject(challenge[field], update[field], destination, field);
  });
  return destination;
}

function updateObject(origVal, updateVal, dest, key) {
  if (typeof (origVal) != 'object' || (typeof (origVal) == 'object' && Array.isArray(origVal) && Array.isArray(updateVal)))
    dest[key] = updateVal;
  else if (typeof (origVal) == 'object' && typeof (updateVal) == 'object') {
    const mergedFields = _.union(Object.keys(origVal), Object.keys(updateVal));
    mergedFields.forEach(function (field) {
      if (!origVal[field] && updateVal[field])
        dest[key][field] = updateVal[field];
      else if (origVal[field] && updateVal[field]) {
        updateObject(origVal[field], updateVal[field], dest[key], field);
      }
    });
  }
}

// Add the allowed create properties on a challenge to the destination object.
function createChallengePostPayload(inputChallengeData, destination) {
  const challengeFields = ['id', 'confidentialityType', 'technologies', 'subTrack',
    'name', 'reviewType', 'billingAccountId', 'milestoneId', 'detailedRequirements',
    'submissionGuidelines', 'registrationStartsAt', 'registrationEndsAt', 'checkpointSubmissionStartsAt',
    'checkpointSubmissionEndsAt', 'submissionEndsAt', 'round1Info', 'round2Info', 'platforms',
    'numberOfCheckpointPrizes', 'checkpointPrize', 'finalDeliverableTypes', 'prizes',
    'projectId', 'submissionVisibility', 'maxNumOfSubmissions', 'task', 'assignees',
    'copilotId', 'copilotFee', 'failedRegisterUsers', 'environment', 'codeRepo', 'fixedFee',
    'percentageFee'];
  challengeFields.forEach(function (field) {
    if (inputChallengeData[field])
      destination[field] = inputChallengeData[field];
  });
}