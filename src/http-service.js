'use strict';

// For creating M2M token, inspiration is taken from notification-processor code
// https://github.com/topcoder-platform/notification-processor/blob/develop/src/common/helper.js
const request = require('request');
const appUtils = require('./utils/helper');
const applicationConfig = appUtils.getApplicationConfig();
const m2mAuth = require('tc-core-library-js').auth.m2m;
const m2m = m2mAuth({
  'AUTH0_URL': applicationConfig.AUTH0_URL,
  'AUTH0_AUDIENCE': applicationConfig.AUTH0_AUDIENCE,
  'TOKEN_CACHE_TIME': applicationConfig.TOKEN_CACHE_TIME
});

// Future work: Put some retry logic for the outgoing http/https calls.

// HTTP service to make all the HTTP Calls
function HttpService(baseUrl) {
  this.baseUrl = baseUrl;
}

module.exports.HttpService = HttpService;

HttpService.prototype.get = function (path, cb) {
  const reqUrl = this.baseUrl + path;
  console.info('[Http Service] GET ReqUrl is:', reqUrl);
  getM2MToken().then(function (token) {
    const options = {
      method: 'GET',
      url: reqUrl,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }
    request(options, function (err, response, body) {
      processRequesCb(err, response, body, cb);
    });
  });
};

HttpService.prototype.post = function (path, body, options, cb) {
  const reqUrl = this.baseUrl + path;
  console.log('[Http Service] POST ReqUrl is:', reqUrl);
  getM2MToken().then(function (token) {
    const options = {
      method: 'POST',
      url: reqUrl,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      },
      json: true,
      body: body
    };
    request(options, function (err, response, body) {
      processRequesCb(err, response, body, cb);
    });
  });
};

HttpService.prototype.put = function (path, body, options, cb) {
  const reqUrl = this.baseUrl + path;
  console.info('[Http Service] PUT ReqUrl is:', reqUrl);
  getM2MToken().then(function (token) {
    const options = {
      method: 'PUT',
      url: reqUrl,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      },
      json: true,
      body: body
    };
    request(options, function (err, response, body) {
      processRequesCb(err, response, body, cb);
    });
  });
};

function getM2MToken() {
  return m2m.getMachineToken(applicationConfig.AUTH0_CLIENT_ID, applicationConfig.AUTH0_CLIENT_SECRET);
}

function processRequesCb(err, response, body, cb) {
  if (err) {
    cb(err);
  } else if (response.statusCode != 200 && response.statusCode != 201) {
    cb('Received response code ' + response.statusCode);
  } else {
    if (typeof (body) === 'string')
      body = JSON.parse(body);
    cb(null, body);
  }
}