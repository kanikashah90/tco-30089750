// Listens to the kafka events
'use strict';
const Kafka = require('node-rdkafka');
const EventEmitter = require('events').EventEmitter;
const utils = require('util');
const appUtils = require('./utils/helper');
const applicationConfig = appUtils.getApplicationConfig();

// Over here create a Kafka Customer that can consume
// the topics. In the topics look for following events
// to take action on:
//  challenge.notification.create
//  challenge.notification.update
function KafkaConsumerImpl(options) {
  this.groupId = (options && options.groupId) ? options.groupId : applicationConfig.KAFKA_CONSUMER_GROUP_ID;
  this.brokerList = (options && options.brokerList) ? options.brokerList : applicationConfig.KAFKA_CONSUMER_BROKER_LIST;
  var self = this;
  if (!this.groupId)
    console.error('Group Id is not defined for the Kafka consumer');
  else if (!this.brokerList)
    console.error('Broker List is not defined for the Kafka consumer');
  else {
    this.consumer = new Kafka.KafkaConsumer({
      'group.id': this.groupId,
      'metadata.broker.list': this.brokerList,
      'enable.auto.commit': false
    });
    this.logger = this.logger();
    this.subscribedTopics = [];
    this.consumer.connect();
    this.consumer.on('ready', function () {
      console.log('[Kafka Consumer] Connected to Kafka');
      self.consumerConnected = true;
      if (options && options.topics) {
        self.subscribeTopic(options.topics);
      } else {
        var defaultTopics = [];
        if (applicationConfig.KAFKA_CHALLENGE_CREATE_TOPIC)
          defaultTopics.push(applicationConfig.KAFKA_CHALLENGE_CREATE_TOPIC);
        if (applicationConfig.KAFKA_CHALLENGE_UPDATE_TOPIC)
          defaultTopics.push(applicationConfig.KAFKA_CHALLENGE_UPDATE_TOPIC);
        self.subscribeTopic(defaultTopics);
      }
      self.emit('consumerReady');
    });

    // Waits to connect with Kafka brokers for a minute. Else it times out
    setTimeout(function () {
      if (!self.consumerConnected)
        self.emit('consumerTimedOut');
    }, applicationConfig.KAFKA_CONNECTION_TIMEOUT_MS);
  }
}

utils.inherits(KafkaConsumerImpl, EventEmitter);

module.exports.KafkaConsumerImpl = KafkaConsumerImpl;

// Sets the subscription topics on the Kafka Consumer
KafkaConsumerImpl.prototype.subscribeTopic = function (topics) {
  if (!Array.isArray(topics)) {
    topics = [topics];
  }
  this.subscribedTopics = this.subscribedTopics.concat(topics)
  this.consumer.subscribe(this.subscribedTopics);
}

// On receipt of the records for subscribed topics
// it emits event which application listens on
// to take further action on.
KafkaConsumerImpl.prototype.consume = function () {
  var self = this;
  this.consumer.on('data', function (record) {
    self.logger.debug('Record is:', record);
    self.logger.info('Record value is:' + record.value.toString());
    self.logger.info('Record topic is:' + record.topic);
    if (record.topic === applicationConfig.KAFKA_CHALLENGE_CREATE_TOPIC) {
      self.emit(applicationConfig.APPLICATION_CHALLENGE_PROCESSOR_CREATED_EVENT, record.value.toString());
    }
    if (record.topic === applicationConfig.KAFKA_CHALLENGE_UPDATE_TOPIC)
      self.emit(applicationConfig.APPLICATION_CHALLENGE_PROCESSOR_UPDATE_EVENT, record.value.toString());
  });
  this.consumer.consume();
}

// Logger function to append all the messages by [Kafka Consumer]
KafkaConsumerImpl.prototype.logger = function () {
  function createMessage(input) {
    var msg = '';
    for (var i = 0; i < input.length; i++) {
      msg += input[i];
    }
    return msg;
  }

  return {
    info: function () {
      console.log('[Kafka Consumer]', createMessage(arguments));
    },
    debug: function () {
      if (applicationConfig.LOG_LEVEL === 'Debug')
        console.log('[Kafka Consumer]', createMessage(arguments));
    }
  }
}