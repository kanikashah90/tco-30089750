const KafkaConsumerImpl = require('./kafka-consumer-impl').KafkaConsumerImpl;
const HttpService = require('./http-service').HttpService;
const ChallengeApiService = require('./challenge-api-service').ChallengeApiService;
const util = require('util');
const appUtils = require('./utils/helper');
const applicationConfig = appUtils.getApplicationConfig();
const EventEmitter = require('events').EventEmitter;

// Main Processor Class for challenge events
function ChallengeProcessor() {
  EventEmitter.call(this);
}

util.inherits(ChallengeProcessor, EventEmitter);

// Initializes all the services that the Processor will use.
//  ChallengeAPI Service -- This Service takes care of the interaction with the Challenge API
//  HTTP Srevice -- This Service takes care of making the HTTP calls.
//  KafkaConsumer -- The KafkaConsumer object on which we listen for data on specific Kafka Topics.
ChallengeProcessor.prototype.init = function () {
  var self = this;
  this.httpService = createHttpService();
  this.challengeService = new ChallengeApiService(this.httpService);
  this.kafkaConsumer = new KafkaConsumerImpl({
    groupId: applicationConfig.KAFKA_CONSUMER_GROUP_ID,
    brokerList: applicationConfig.KAFKA_CONSUMER_BROKER_LIST
  });
  this.kafkaConsumer.on('consumerReady', function () {
    self.registerChallengeEvents();
    self.kafkaConsumer.consume();
  });
  this.kafkaConsumer.on('consumerTimedOut', function () {
    console.log('Consumer failed in connecting to Kafka brokers', applicationConfig.KAFKA_CONSUMER_BROKER_LIST);
    process.exit(0);
  });
}

// Registers the Kafka Consumer events to make corresponding calls to Topcoder API
// through Challenge API Service
ChallengeProcessor.prototype.registerChallengeEvents = function () {
  var self = this;
  self.kafkaConsumer.on(applicationConfig.APPLICATION_CHALLENGE_PROCESSOR_CREATED_EVENT, function (challengeString) {
    var challengeJson = parseChallengeString(challengeString);
    console.log('Challenfe JSON');
    console.log(challengeJson);
    if (challengeJson) {
      self.challengeService.create(challengeJson, function (err, data) {
        if (err) {
          console.error(err);
        } else {
          console.log(data);
          self.emit('challenge.created.success', data);
        }
      });
    }
    else
      console.info('Malformed JSON. Skipping the creation of challenge', challengeString);
  });
  self.kafkaConsumer.on(applicationConfig.APPLICATION_CHALLENGE_PROCESSOR_UPDATE_EVENT, function (challengeString) {
    var challengeJson = parseChallengeString(challengeString);
    if (challengeJson) {
      self.challengeService.update(challengeJson, function (err, data) {
        if (err)
          console.error(err);
        else {
          console.log(data);
          self.emit('challenge.updated.success', data);
        }
      });
    }
    else
      console.info('Malformed JSON. Skipping the updat to the challenge', challengeString);
  });
}

var processorIns = new ChallengeProcessor();
processorIns.init();

module.exports.processorIns = processorIns;

// Parses String to JSON
function parseChallengeString(challengeString) {
  try {
    const challengeJSON = JSON.parse(challengeString);
    return challengeJSON;
  } catch (e) {
    console.error('Error in parsing challege string to json', challengeString, e);
    return;
  }
}

// Returns an instance of HTTP Service
function createHttpService() {
  const baseUrl = process.env.CHALLENGE_URL ? process.env.CHALLENGE_URL : applicationConfig.DEFAULT_CHALLENGE_URL;
  const apiVersion = process.env.API_VERSION ? process.env.API_VERSION : applicationConfig.API_VERSION;
  return new HttpService(baseUrl + apiVersion + applicationConfig.CHALLENGE_PATH + '/');
}