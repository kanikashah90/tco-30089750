module.exports = {
  getApplicationConfig: function () {
    var applicationConfig = require('../../config/default');
    var envApplicationConfig;
    if (process.env.TC_TEST) {
      envApplicationConfig = require('../../config/test');
    } else if (process.env.TC_PROD) {
      envApplicationConfig = require('../../config/production');
    }
    if (envApplicationConfig) {
      for (var prop in envApplicationConfig) {
        applicationConfig[prop] = envApplicationConfig[prop];
      }
    }
    return applicationConfig;
  }
}