const Kafka = require('node-rdkafka');
const KafkaConsumerImpl = require('../../src/kafka-consumer-impl').KafkaConsumerImpl;
const applicationConfig = require('../../src/utils/helper').getApplicationConfig();

describe('Kafka Consumer Implementation', function () {
  describe('Constructor', function () {
    it('should fetch the Kafka consumer configuration from options', function (done) {
      const consumerOptions = {
        groupId: 'testId',
        brokerList: 'testList:2131'
      };
      const kafkaConsumer = new KafkaConsumerImpl(consumerOptions);
      expect(kafkaConsumer.groupId).toEqual(consumerOptions.groupId);
      expect(kafkaConsumer.brokerList).toEqual(consumerOptions.brokerList);
      done();
    });

    it('should default the Kafka consumer configuration to config variables', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      expect(kafkaConsumer.groupId).toEqual(applicationConfig.KAFKA_CONSUMER_GROUP_ID);
      expect(kafkaConsumer.brokerList).toEqual(applicationConfig.KAFKA_CONSUMER_BROKER_LIST);
      done();
    });

    it('should call the connect function of Kafka library', function (done) {
      var connectStub = jasmine.createSpy();
      spyOn(Kafka, 'KafkaConsumer').and.returnValue({
        connect: connectStub,
        on: function () { }
      });
      new KafkaConsumerImpl();
      expect(connectStub).toHaveBeenCalled();
      done();
    });

    it('should listen to the ready event on the Kafka Consumer created', function (done) {
      var onSpy = jasmine.createSpy();
      spyOn(Kafka, 'KafkaConsumer').and.returnValue({
        connect: jasmine.createSpy(),
        on: onSpy
      });
      new KafkaConsumerImpl();
      expect(onSpy.calls.argsFor(0)[0]).toEqual('ready');
      done();
    });

    it('should call subscribe topic method with the challenge create and update notification topics once Kafka consumer is ready', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      spyOn(kafkaConsumer, 'subscribeTopic').and.returnValue();
      kafkaConsumer.consumer.emit('ready', {});
      process.nextTick(function () {
        expect(kafkaConsumer.subscribeTopic.calls.argsFor(0)[0]).toEqual([applicationConfig.KAFKA_CHALLENGE_CREATE_TOPIC, applicationConfig.KAFKA_CHALLENGE_UPDATE_TOPIC]);
        expect(kafkaConsumer.subscribeTopic).toHaveBeenCalled();
        done();
      });
    });

    it('should emit consumerReady event once Kafka consumers ready event is processed', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      spyOn(kafkaConsumer, 'emit').and.returnValue();
      spyOn(kafkaConsumer, 'subscribeTopic').and.returnValue();
      kafkaConsumer.consumer.emit('ready', {});
      process.nextTick(function () {
        expect(kafkaConsumer.emit.calls.argsFor(0)[0]).toEqual('consumerReady');
        done();
      });
    });
  });

  describe('#subscribeTopic', function () {
    it('should accept single string argument for topic', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      expect(kafkaConsumer.subscribedTopics).toEqual([]);
      spyOn(kafkaConsumer.consumer, 'subscribe').and.returnValue();
      kafkaConsumer.subscribeTopic('testTopic');
      expect(kafkaConsumer.consumer.subscribe).toHaveBeenCalled();
      expect(kafkaConsumer.subscribedTopics).toEqual(['testTopic']);
      done();
    });

    it('should accept array argument for topics', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      expect(kafkaConsumer.subscribedTopics).toEqual([]);
      spyOn(kafkaConsumer.consumer, 'subscribe').and.returnValue();
      kafkaConsumer.subscribeTopic(['testTopic1', 'testTopic2']);
      expect(kafkaConsumer.consumer.subscribe).toHaveBeenCalled();
      expect(kafkaConsumer.subscribedTopics).toEqual(['testTopic1', 'testTopic2']);
      done();
    });

    it('should call Kafka consumers subsrcibe method with the topics', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      expect(kafkaConsumer.subscribedTopics).toEqual([]);
      spyOn(kafkaConsumer.consumer, 'subscribe').and.returnValue();
      kafkaConsumer.subscribeTopic(['testTopic1', 'testTopic2']);
      expect(kafkaConsumer.consumer.subscribe).toHaveBeenCalled();
      expect(kafkaConsumer.consumer.subscribe.calls.argsFor(0)[0]).toEqual(['testTopic1', 'testTopic2']);
      done();
    });
  });

  describe('#consumer', function () {
    it('should register data event handler on Kafka Consumer', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      spyOn(kafkaConsumer.consumer, 'on').and.returnValue();
      kafkaConsumer.consume();
      expect(kafkaConsumer.consumer.on).toHaveBeenCalled();
      done();
    });

    it('should emit application.processor.challenge.created when Kafka consumer gets data for challenge creation notification', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      spyOn(kafkaConsumer, 'emit').and.returnValue();
      const createRecord = {
        topic: applicationConfig.KAFKA_CHALLENGE_CREATE_TOPIC,
        value: Buffer.from('test value', 'utf8')
      };
      kafkaConsumer.consume();
      kafkaConsumer.consumer.emit('data', createRecord);
      expect(kafkaConsumer.emit.calls.count()).toEqual(1);
      expect(kafkaConsumer.emit.calls.argsFor(0)).toEqual([
        applicationConfig.APPLICATION_CHALLENGE_PROCESSOR_CREATED_EVENT,
        'test value'
      ]);
      done();
    });

    it('should emit application.processor.challenge.updated when Kafka consumer gets update for challenge creation notification', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      spyOn(kafkaConsumer, 'emit').and.returnValue();
      const createRecord = {
        topic: applicationConfig.KAFKA_CHALLENGE_UPDATE_TOPIC,
        value: Buffer.from('test update', 'utf8')
      };
      kafkaConsumer.consume();
      kafkaConsumer.consumer.emit('data', createRecord);
      expect(kafkaConsumer.emit.calls.count()).toEqual(1);
      expect(kafkaConsumer.emit.calls.argsFor(0)).toEqual([
        applicationConfig.APPLICATION_CHALLENGE_PROCESSOR_UPDATE_EVENT,
        'test update'
      ]);
      done();
    });

    it('should call consume method of Kafka Consumer', function (done) {
      const kafkaConsumer = new KafkaConsumerImpl();
      spyOn(kafkaConsumer.consumer, 'consume').and.returnValue();
      kafkaConsumer.consume();
      expect(kafkaConsumer.consumer.consume).toHaveBeenCalled();
      done();
    });
  });
});