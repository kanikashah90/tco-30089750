const Jasmine = require('jasmine');
const JasmineConsoleReporter = require('jasmine-console-reporter');
const jasmine = new Jasmine();
var reporter = new JasmineConsoleReporter({
  colors: 1,
  cleanStack: 3,
  verbosity: 4,
  listStyle: 'indent',
  activity: false
});
jasmine.addReporter(reporter);
jasmine.showColors(true);
if (process.env.TEST_UNIT)
  jasmine.loadConfigFile('spec/unit/jasmine.json');
if (process.env.TEST_E2E)
  jasmine.loadConfigFile('spec/e2e/jasmine.json');
jasmine.execute();