const KafkaProducerImpl = require('../../src/kafka-producer-impl').KafkaProducerImpl;
const samples = require('./samples');
const appUtils = require('../../src/utils/helper');
const applicationConfig = appUtils.getApplicationConfig();
var processorIns;

function loadProcessor() {
  const processor = require('../../src/index');
  processorIns = processor.processorIns;
}

const kafkaProducer = new KafkaProducerImpl({
  brokerList: applicationConfig.KAFKA_CONSUMER_BROKER_LIST
});

describe('Create Challenge', function () {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000 * 20;
  beforeAll(function (done) {
    kafkaProducer.on('producerReady', function () {
      loadProcessor();
      done();
    })
  });
  it('Challenge processor should emit challenge.created.success event when challenge.notification.create is consumed by kafka consumer', function (done) {
    processorIns.on('challenge.created.success', function () {
      done();
    });
    setTimeout(function () {
      kafkaProducer.produce('challenge.notification.create', JSON.stringify(samples.createChallenge), 'challenge.notification.create');
    }, 7000);
  });

  it('Challenge processor should emit challenge.updated.success event when challenge.notification.create is consumed by kafka consumer', function (done) {
    processorIns.on('challenge.updated.success', function () {
      done();
    });
    setTimeout(function () {
      kafkaProducer.produce('challenge.notification.update', JSON.stringify(samples.updateChallenge), 'challenge.notification.update');
    }, 7000);
  });
})
