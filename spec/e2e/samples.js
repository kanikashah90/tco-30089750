module.exports = {
  createChallenge: {
    confidentialityType: 'public',
    subTrack: 'FIRST_2_FINISH',
    name: 'Test F2F Challenge',
    reviewType: 'INTERNAL',
    milestoneId: 22,
    registrationStartsAt: '2018-09-03T15:32:18.767Z',
    projectId: 26
  },
  updateChallenge: {
    id: 30052924,
    prizes: [15]
  }
}