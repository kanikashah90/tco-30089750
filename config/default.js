module.exports = {
  'KAFKA_CONSUMER_GROUP_ID': process.env.KAFKA_CONSUMER_GROUP_ID || 'tc-challenge-processor-group-default',
  'KAFKA_CONSUMER_BROKER_LIST': process.env.KAFKA_CONSUMER_BROKER_LIST || 'localhost:9092',
  'KAFKA_CHALLENGE_CREATE_TOPIC': 'challenge.notification.create',
  'KAFKA_CHALLENGE_UPDATE_TOPIC': 'challenge.notification.update',
  'APPLICATION_CHALLENGE_PROCESSOR_CREATED_EVENT': 'application.processor.challenge.created',
  'APPLICATION_CHALLENGE_PROCESSOR_UPDATE_EVENT': 'application.processor.challenge.updated',
  'KAFKA_CONNECTION_TIMEOUT_MS': 60 * 1000, // one minute
  'AUTH0_URL': 'https://topcoder-dev.auth0.com/oauth/token',
  'AUTH0_AUDIENCE': 'https://m2m.topcoder-dev.com/',
  'TOKEN_CACHE_TIME': 90,
  'AUTH0_CLIENT_ID': 'e6oZAxnoFvjdRtjJs1Jt3tquLnNSTs0e',
  'AUTH0_CLIENT_SECRET': 'OGCzOnQkhYTQpZM3NI0sD--JJ_EPcm2E7707_k6zX11m223LrRK1-QZL4Pon4y-D',
  'DEFAULT_CHALLENGE_URL': 'https://api.topcoder-dev.com',
  'API_VERSION': '/v4',
  'CHALLENGE_PATH': '/challenges',
  'LOG_LEVEL': process.env.DEBUG_MODE ? 'Debug' : 'Info'
}