module.exports = {
  'KAFKA_CONSUMER_GROUP_ID': process.env.KAFKA_CONSUMER_GROUP_ID || 'tc-challenge-processor-group-prod',
}