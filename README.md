# Topcoder - Challenge Notification Processor

## Dependencies

- nodejs https://nodejs.org/en/ (v8+)
- Kafka
- Docker, Docker Compose

## Configuration

Configuration for the notification server is at `config/default.js`.
The following parameters can be set in config files or in env variables:

- LOG_LEVEL: the log level; default value: 'Info'
- KAFKA_CONSUMER_BROKER_LIST: comma separated Kafka hosts for consumer to listen; default value: 'localhost:9092'
- KAFKA_CONSUMER_GROUP_ID: consumer group id; default value: 'tc-challenge-processor-group-default'
- KAFKA_CHALLENGE_CREATE_TOPIC: Challenge notification create Kafka topic, default value is 'challenge.notification.create'
- KAFKA_CHALLENGE_UPDATE_TOPIC: Challenge notification update Kafka topic, default value is 'challenge.notification.update'
- APPLICATION_CHALLENGE_PROCESSOR_CREATED_EVENT: Event created by the application when it receives kafka challenge creation event, default value is 'application.processor.challenge.created'
- APPLICATION_CHALLENGE_PROCESSOR_UPDATE_EVENT: Event created by the application when it receives kafka challenge update event, default value is 'application.processor.challenge.updated'
- KAFKA_CONNECTION_TIMEOUT_MS: Number of millisecconds Kafka consumer waits to form connection with Kafka brokers. Default to 1 minute.
- CHALLENGE_API_URL: Challenge API URL, default value is 'https://api.topcoder-dev.com'
- API_VERSION: Topcoder API version, default value is '/v4'
- CHALLENGE_PATH: Path to challneges in Challenge API, default value '/challenges'
- AUTH0_URL: Auth0 url for M2M token
- AUTH0_AUDIENCE: Auth0 audience for M2M token || 'https://m2m.topcoder-dev.com/',
- TOKEN_CACHE_TIME: Cache time of M2M token, optional
- AUTH0_CLIENT_ID: Auth0 client id for M2M token
- AUTH0_CLIENT_SECRET: Auth0 client secret for M2M token


Configuration for the tests is at `config/test.js`. Following parameters need to be set via environment variables or directly in config file

## Local Kafka setup

- `http://kafka.apache.org/quickstart` contains details to setup and manage Kafka server,
  below provides details to setup Kafka server in Mac, Windows will use bat commands in bin/windows instead
- download kafka at `https://www.apache.org/dyn/closer.cgi?path=/kafka/1.1.0/kafka_2.11-1.1.0.tgz`
- extract out the downloaded tgz file
- go to extracted directory kafka_2.11-0.11.0.1
- start ZooKeeper server:
  `bin/zookeeper-server-start.sh config/zookeeper.properties`
- use another terminal, go to same directory, start the Kafka server:
  `bin/kafka-server-start.sh config/server.properties`
- note that the zookeeper server is at localhost:2181, and Kafka server is at localhost:9092
- use another terminal, go to same directory, create some topics:
  `bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic submission.notification.create`
  `bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic submission.notification.update`
   `bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test.email`
- verify that the topics are created:
  `bin/kafka-topics.sh --list --zookeeper localhost:2181`,
  it should list out the created topics

## Local deployment
- Configure the CPP and LD flags as mentioned here: https://github.com/Blizzard/node-rdkafka
```
export CPPFLAGS=-I/usr/local/opt/openssl/include
export LDFLAGS=-L/usr/local/opt/openssl/lib
```
- install dependencies `npm i`
- start processor app `npm start`

## Local Deployment with Docker

To run the processor using docker, follow the below steps

#### Build the Docker image
1. Run the following command from project root directory:

```
docker build --tag challenge-notification-processor:latest --file Docker/Dockerfile .
```

#### Run the processor in locally in Docker

1. Navigate to the directory `Docker`

2. Rename the file `sample.api.env` to `api.env`

3. Set parameters in the file `api.env`  
Example: KAFKA_CONSUMER_BROKER_LIST=localhost:9092  
Note: The docker container creation configuration uses host network. On Mac, docker containers cannot map localhost to Mac.
You can find the local ip of Mac for the docker containers by running this command inside docker container with network configured as host.
```
ping docker.for.mac.localhost
```

4. Once that is done, run the following command

```
docker-compose up
```


## Running unit and e2e tests with coverage


To run unit tests with coverage report
```
npm run test-unit
```

To run e2e tests with coverage report
```
npm run test-e2e
```

Coverage results are generated in folder `coverage/lcov-report/`

README Reference: https://raw.githubusercontent.com/topcoder-platform/notification-processor/develop/README.md